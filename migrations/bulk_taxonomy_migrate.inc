<?php

/**
 * Class BulkTaxonomyMigrate.
 */
class BulkTaxonomyMigrate extends Migration {
  /**
   * Initialization of a Migration object.
   *
   * @var array
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Bulk Taxonomy migration import mechanism.');

    // Setting of the primary key.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'uuid'  => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Taxonomy terms',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Load the current being migrated file.
    $source_file = variable_get('bulk_taxonomy_migrate_single_file_path', '');
    $machine_name = $this->getMachineNameFromSingleFilePath($source_file);

    $this->source = new MigrateSourceCSV($source_file, $this->csvcolumns(), array('header_rows' => 1));

    // Setting the destination to an existing voc. We need to allow duplicate
    // terms for translation. Eg: FR: Aluminium EN: Aluminium.
    $term_options['allow_duplicate_terms'] = TRUE;
    $this->destination = new MigrateDestinationTerm($machine_name, $term_options);

    // Field mapping.
    $this->addFieldMapping('uuid', 'uuid');
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('language', 'language');

    // All the fields we don’t want to migrate.
    $this->addUnmigratedDestinations(
      array(
        'tid',
        'description',
        'parent',
        'parent_name',
        'format',
        'weight',
        'path',
        'pathauto',
        'machine_name',
      )
    );
  }

  /**
   * Function to create the vocabulary if it doesn't exist yet.
   */
  public function preImport() {
    parent::preImport();
    $source_file = variable_get('bulk_taxonomy_migrate_single_file_path', NULL);

    $machine_name = $this->getMachineNameFromSingleFilePath($source_file);
    $human_name = $this->getVocNameFromSingleFilePath($source_file);

    if (!taxonomy_vocabulary_machine_name_load($machine_name)) {
      $vocabulary = new stdClass();
      $vocabulary->name = $human_name;
      $vocabulary->machine_name = $machine_name;

      taxonomy_vocabulary_save($vocabulary);
    }
  }

  /**
   * Function to prepare the rows before importing.
   *
   * @param object $row
   *   The current row being imported.
   *
   * @return bool
   *   Returns if successful or not.
   */
  public function prepareRow($row) {
    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $default_language = language_default();
    $row->language = variable_get('bulk_taxonomy_migrate_language', $default_language->language);

    // If our language is not default, we need to migrate the translation.
    if ($row->language != $default_language->language) {
      $source_file = variable_get('bulk_taxonomy_migrate_single_file_path', NULL);
      $voc_machine_name = $this->getMachineNameFromSingleFilePath($source_file);
      $voc = taxonomy_vocabulary_machine_name_load($voc_machine_name);
      $term = $this->getTermByUuid($row->uuid, $voc->machine_name);

      // Only translate if we have a source term.
      if (isset($term)) {
        i18n_string_translation_update(
          array('taxonomy', 'term', $term->tid, 'name'),
          $row->name,
          $row->language,
          $term->name
        );
      }

      // We return false because we don't need to "migrate" the term itself if
      // we are migrating the translations.
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Map the CSV columns.
   *
   * @return array array
   *   Returns an array of csv columns.
   */
  protected function csvcolumns() {
    $columns = array(
      0 => array('uuid', 'uuid'),
      1 => array('name', 'name'),
    );
    return $columns;
  }

  /**
   * Function to get the machine name from the source file path.
   *
   * @param string $path
   *   A string containing the path to the CSV file.
   *   Example: ".../Colour #colour.csv".
   *
   * @return string
   *   Returns the machine name from the file name.
   *   Example: "colour".
   */
  protected function getMachineNameFromSingleFilePath($path) {
    $machine_name_with_extensions = explode('#', $path);
    $machine_name_with_extensions = end($machine_name_with_extensions);
    // We split on all dots (machine name itself does not contain any).
    // Needed because the # part could look like '#colour.fr.csv' .
    $exploded_machine_name = explode('.', $machine_name_with_extensions);
    $machine_name = reset($exploded_machine_name);
    return $machine_name;
  }

  /**
   * Function to get the vocabulary name from the source file path.
   *
   * Used by the taxonomy migration.
   *
   * @param string $path
   *   A string containing the path to the CSV file.
   *   Example: ".../Access - Credentials - Series #acc_credentials_series.csv".
   *
   * @return string
   *   Returns Human readable name from filename
   *   Example: "Access - Credentials - Series" .
   */
  protected function getVocNameFromSingleFilePath($path) {
    $explode = explode('/', $path);
    $fullname = array_pop($explode);
    return rtrim(substr($fullname, 0, strpos($fullname, '#')));
  }

  /**
   * Get term by UUID.
   *
   * @param string $uuid
   *   The term UUID.
   * @param null|object $vocabulary
   *   The vocabulary object, default null.
   *
   * @return object
   *   Array of terms, but because we fetch by UUID, it's an array of one.
   */
  protected function getTermByUuid ($uuid, $vocabulary = NULL) {
    $conditions = array('uuid' => $uuid);
    if (isset($vocabulary)) {
      $vocabularies = taxonomy_vocabulary_get_names();
      if (isset($vocabularies[$vocabulary])) {
        $conditions['vid'] = $vocabularies[$vocabulary]->vid;
      }
      else {
        // Return an empty array when filtering by a non-existing vocabulary.
        return array();
      }
    }
    return reset(taxonomy_term_load_multiple(array(), $conditions));
  }
}