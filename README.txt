INTRODUCTION
------------

This module enables you to bulk migrate taxonomy terms and vocabularies based upon a simple CSV file.

FEATURES
--------

 * Create and/or update taxonomy terms based of CSV files. If the vocabulary does not exist, it will be created.
 * Migrate translations of terms.