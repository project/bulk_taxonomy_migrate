<?php

/**
 * Implements hook_drush_command().
 */
function bulk_taxonomy_migrate_drush_command() {
  $items['migrate_taxonomy'] = [
    'description' => 'Migrate all the taxonomy terms from a specific path',
    'callback' => 'drush_bulk_taxonomy_migrate_import_all',
    'aliases' => ['mt'],
    'arguments' => [
      'subdir' => 'If you have your terms CSV\'s divided in sub-folders, you can specify this directory to only run the migrations on those files.',
    ],
    'options' => [
      'language' => 'The language we want to migrate the files for. If not supplied default site language will be used.'
    ]
  ];

  $items['taxonomy-set-translatable-mode'] = [
    'description' => 'Configures the translation mode for all taxonomies',
    'callback' => 'drush_bulk_taxonomy_migrate_update_translation_mode',
    'aliases' => ['tstm'],
  ];

  return $items;
}


/**
 * Callback function for bulk_taxonomy_migrate_drush_command.
 *
 * @param string | bool $subdir
 *   The subdir argument, set to FALSE if not provided.
 */
function drush_bulk_taxonomy_migrate_import_all($subdir = FALSE) {
  $path = variable_get('bulk_taxonomy_migrate_files_path', '');

  // Validate the configured path.
  if (!is_dir(DRUPAL_ROOT . DIRECTORY_SEPARATOR . $path)) {
    return drush_set_error(
      dt('"@path" is not a valid directory.', ['@path' => $path])
    );
  }

  // Validate the language option.
  $language_option = drush_get_option('language', NULL);
  if (!empty($language_option)) {
    include_once DRUPAL_ROOT . '/includes/iso.inc';
    $iso_languages = _locale_get_predefined_list();
    // Installed custom languages.
    $custom_languages = language_list($field = 'language');

    // Check the iso languages.
    $iso_language_found = array_key_exists($language_option, $iso_languages);
    // Check custom languages.
    $custom_language_found = array_key_exists($language_option, $custom_languages);

    if (!$iso_language_found && !$custom_language_found) {
      return drush_set_error(
        dt('"@lang" is not a valid language. Please choose between one of the ISO codes or custom defined languages.', array('@lang' => $language_option))
      );
    }
  }

  // Validate the subdir if it's provided.
  if($subdir) {
    $path .= DIRECTORY_SEPARATOR . $subdir;
    if (!is_dir(DRUPAL_ROOT . DIRECTORY_SEPARATOR . $path)) {
      return drush_set_error(
        dt('"@path" is not a valid directory.', ['@path' => $path])
      );
    }
  }

  $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(drupal_realpath($path)), RecursiveIteratorIterator::SELF_FIRST);
  foreach ($objects as $name => $object) {
    // Get the full filename eg: "Colour #colour.fr".
    $filename = pathinfo($object, PATHINFO_FILENAME);
    // Split it up on '.' and get the language extension if set.
    $language_extension = end(explode('.', $filename));
    $default_language = language_default();

    // Load the current migration language.
    $migration_language = drush_get_option('language', $default_language->language);
    variable_set('bulk_taxonomy_migrate_language', $migration_language);
    // Migrate file variable.
    $migrate_file = FALSE;

    // If we didn't provide a migration language, or if it's set to the site's
    // default we migrate the default language files.
    if (!is_null($migration_language) && $migration_language == $default_language->language) {
      $migrate_file = TRUE;
      // Check if there is a language extension.
      $machine_name_with_extensions = end(explode('#', $filename));
      if (strpos($machine_name_with_extensions, '.')) {
        $migrate_file = FALSE;
      }
    }
    elseif ($language_extension == $migration_language) {
      $migrate_file = TRUE;
    }

    if ((pathinfo($object, PATHINFO_EXTENSION) == 'csv') && (($handle = fopen($name, "r")) !== FALSE) && $migrate_file) {
      print '**** Importing: ' . basename($name) . " **** \n";

      $file_path = $object->getPathname();
      variable_set('bulk_taxonomy_migrate_single_file_path', $file_path);

      $command = 'migrate-import';
      $args = ['BulkTaxonomy'];
      $options = ['--update'];
      drush_invoke_process('@self', $command, $args, $options);
      fclose($handle);
    }
  }
  return drush_log(t('Done processing all taxonomy files for language: !language', array('!language' => variable_get(MIGRATE_LANGUAGE_VARIABLE))), 'success');
}

/**
 * Callback function to update all vocabularies translation modes.
 */
function drush_bulk_taxonomy_migrate_update_translation_mode() {
  if (drush_confirm('WARNING: This command will configure the same (I18N_MODE_LOCALIZE) translation mode for -- all -- vocabularies! Do you want to proceed?')) {
    $vocabularies = taxonomy_get_vocabularies();
    print 'Updating all vocabularies to mode ' . I18N_MODE_LOCALIZE . "\n";
    foreach ($vocabularies as $vocabulary) {
      $vocabulary->i18n_mode = I18N_MODE_LOCALIZE;
      taxonomy_vocabulary_save($vocabulary);
      print 'Vocabulary "' . $vocabulary->name . '" updated' . "\n";
    }
  }
  else {
      drush_user_abort();
  }
}
