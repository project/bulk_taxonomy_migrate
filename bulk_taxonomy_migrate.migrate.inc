<?php

/**
 * Implements hook_migrate_api().
 */
function bulk_taxonomy_migrate_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'bulk_taxonomy_migrate' => array(
        'title' => 'Bulk Taxonomy Migrate',
      ),
    ),
    'migrations' => array(
      'BulkTaxonomy' => array(
        'class_name' => 'BulkTaxonomyMigrate',
        'group_name' => 'bulk_taxonomy_migrate',
      ),
    ),
  );

  return $api;
}