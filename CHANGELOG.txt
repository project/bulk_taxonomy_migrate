Bulk Taxonomy migrate 1.0-beta1
===============================
#2911361: D7: Basic migrate layout
#2911362: D7: Create configuration screen
#2911363: D7: Create the migration class to migrate multiple CSV's
#2911364: D7: Extend migration class with translation support.
#2911365: D7: Create an example